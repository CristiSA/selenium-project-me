package Blog.SeleniumIntro;

import org.apache.commons.lang3.SystemUtils;
import org.junit.Test;

public class DriverUtils {

    public void setWebdriverProperty(String browser) {
        switch (browser) {
            case "chrome":
                System.setProperty("webdriver.chrome.driver", getDriversPath() + "chromedriver");
                break;
            case "firefox":
                System.setProperty("webdriver.gecko.driver", getDriversPath() + "geckodriver");
                break;

        }
    }

    private String getProjectPath() {
        return System.getProperty("user.dir");
    }


    @Test
    public void testMethods() {
        System.out.println(getProjectPath());

    }

    private String getDriversPath() {
        String driversPath;
        if (SystemUtils.IS_OS_WINDOWS) {
            driversPath = "\\src\\test\\resources\\drivers\\windows";
        }else {
            driversPath = "\\src\\test\\resources\\drivers\\mac";
        }
        return getProjectPath() + "\\src\\test\\resources\\drivers\\windows";
    }

    }


