package Blog.SeleniumIntro;

import org.apache.commons.lang3.SystemUtils;
import org.junit.Test;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.htmlunit.HtmlUnitDriver;

import java.io.IOException;

import static org.junit.Assert.assertEquals;

public class MyFirstTest {
    @Test
    public void openBrowser() throws InterruptedException {
        System.setProperty("webdriver.chrome.driver", "C:\\Users\\Seki\\IdeaProjects\\selenium-project-me\\src\\test\\resources\\drivers\\windows\\chromedriver.exe");
        WebDriver driver = new ChromeDriver();
        driver.get("https://practica.wantsome.ro/blog");
        String pageTitle = driver.getTitle();
        System.out.println(pageTitle);
        Thread.sleep(5000);
        driver.close();
        assertEquals("Wantsome Iasi – Practice website for testing sessions", driver.getTitle());
    }

    @Test
    public void openBrowser2() throws IOException, InterruptedException {
        System.setProperty("webdriver.gecko.driver", "C:\\Users\\Seki\\IdeaProjects\\selenium-project-me\\src\\test\\resources\\drivers\\windows\\geckodriver.exe");
        WebDriver driver = new FirefoxDriver();
        driver.get("https://practica.wantsome.ro/blog");
        String pageTitle = driver.getTitle();
        System.out.println(pageTitle);
        Thread.sleep(5000);

        driver.close();


    }

    @Test
    public void openBrowser3() throws IOException, InterruptedException {
        WebDriver driver = new HtmlUnitDriver();
        driver.get("https://practica.wantsome.ro/blog");
        String pageTitle = driver.getTitle();
        System.out.println(pageTitle);
        Thread.sleep(5000);

        driver.close();
    }

}







